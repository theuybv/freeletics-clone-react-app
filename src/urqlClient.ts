import { createClient, dedupExchange, fetchExchange } from "urql";
import { offlineExchange } from "@urql/exchange-graphcache";
import { GRAPHQL_ENDPOINT_URI } from "./configs";
import schema from "./schema.json";

const makeLocalStorage = () => {
  const cache = {};
  return {
    writeData(delta: any) {
      return Promise.resolve().then(() => {
        Object.assign(cache, delta);
        localStorage.setItem("data", JSON.stringify(cache));
      });
    },
    readData() {
      return Promise.resolve().then(() => {
        const local = localStorage.getItem("data") || null;
        Object.assign(cache, JSON.parse(local as any));
        return cache;
      });
    },
    writeMetadata(data: any) {
      localStorage.setItem("metadata", JSON.stringify(data));
    },
    readMetadata() {
      return Promise.resolve().then(() => {
        const metadataJson = localStorage.getItem("metadata") || null;
        return JSON.parse(metadataJson as any);
      });
    },
    onOnline(cb: () => void) {
      window.addEventListener("online", () => {
        cb();
      });
    },
  };
};

const storage = makeLocalStorage();

const cache = offlineExchange({
  schema,
  storage,
  updates: {
    /* ... */
  },
  optimistic: {
    /* ... */
  },
} as any);

const urqlClient = createClient({
  url: GRAPHQL_ENDPOINT_URI,
  exchanges: [dedupExchange, cache, fetchExchange],
});
export default urqlClient;

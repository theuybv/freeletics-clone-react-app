import React, { useEffect } from "react";
import { Redirect, Route, Router } from "react-router-dom";
import {
  IonApp,
  useIonRouter,
  IonRefresher,
  IonRefresherContent,
  IonContent,
  IonText,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import "./App.css";
/* Theme variables */
import "./theme/variables.css";
import DoneWithWorkout from "./pages/DoneWithWorkout";
import GiveFeedbackWorkout from "./pages/GiveFeedbackWorkout";
import { useStopwatch } from "react-timer-hook";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import {
  WorkoutDetailsFetchComp,
  WorkoutCountDownFetchComp,
  ActiveWorkoutExerciseFetchComp,
  ExploreTabFetchComp,
} from "./components/fetch-data";
import { AppState } from "./reducers/appReducer";
import AppLoader from "./components/AppLoader";
import useToast from "./hooks/useToast";
import { RefresherEventDetail } from "@ionic/core";
import { chevronDownCircleOutline } from "ionicons/icons";

function doRefresh(event: CustomEvent<RefresherEventDetail>) {
  window.location.href = `/explore`;

  // console.log("Begin async operation");

  // window.location.reload();
  // setTimeout(() => {
  //   console.log("Async operation has ended");

  //   event.detail.complete();
  // }, 2000);
}

const App: React.FC = () => {
  const workoutStopWatch = useStopwatch({ autoStart: false });

  const { appIsLoading, appError } = useSelector(
    ({ appIsLoading, appError }: AppState) => {
      return {
        appIsLoading,
        appError,
      };
    },
    shallowEqual
  );

  const dispatch = useDispatch();

  const { openToast, toast } = useToast();

  useEffect(() => {
    dispatch({
      type: "UPDATE_WORKOUT_STOPWATCH",
      payload: { workoutStopWatch },
    });

    return () => {};
  }, [workoutStopWatch]);

  useEffect(() => {
    if (appError) {
      // openToast({
      //   message: JSON.stringify(appError, null, 4),
      //   buttons: [
      //     {
      //       handler: () => {
      //         window.location.href = "/explore";
      //       },
      //     },
      //     {
      //       text: "Refresh",
      //       handler: () => {
      //         window.location.href = "/explore";
      //       },
      //     },
      //   ],
      // });
    }
    return () => {};
  }, [appError]);

  return (
    <IonApp>
      <IonContent>
        {appError && (
          <IonRefresher slot="fixed" onIonRefresh={doRefresh}>
            <IonRefresherContent
              pullingIcon={chevronDownCircleOutline}
              pullingText="Pull to refresh"
              refreshingSpinner="circles"
              refreshingText="Refreshing..."
            ></IonRefresherContent>
            <IonText>
              <h1>Pull to refresh</h1>
            </IonText>
          </IonRefresher>
        )}
        {toast}
        {appIsLoading && <AppLoader />}
        <IonReactRouter>
          <Route
            path="/"
            render={() => <Redirect to="/explore" />}
            exact={true}
          />
          <Route path="/explore" component={ExploreTabFetchComp} exact={true} />
          <Route
            path="/workouts/:workoutId"
            component={WorkoutDetailsFetchComp}
            exact={true}
          />
          <Route
            path="/workouts/:workoutId/countdown"
            component={WorkoutCountDownFetchComp}
            exact={true}
          />
          <Route
            path="/workouts/:workoutId/round/:roundNumber/:exerciseId"
            component={ActiveWorkoutExerciseFetchComp}
            exact={true}
          />
          <Route
            path="/workouts/:workoutId/done"
            component={DoneWithWorkout}
            exact={true}
          />
          <Route
            path="/workouts/:workoutId/feedback"
            component={GiveFeedbackWorkout}
            exact={true}
          />
        </IonReactRouter>
      </IonContent>
    </IonApp>
  );
};

export default App;

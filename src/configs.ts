const configs = {
  development: {
    MEDIA_BASE_URL: `https://www.theuy.nl/freeletics-media/`,
    VIDEOS_BASE_URL: `https://www.theuy.nl/freeletics-media/videos/`,
    THUMBS_BASE_URL: `https://www.theuy.nl/freeletics-media/thumbs/`,
    WORKOUT_COUNTDOWN_SECONDS: 5,
    GRAPHQL_ENDPOINT_URI: `http://localhost:8080/v1/graphql`,
    DIGITAL_PORTFOLIO_LINK: `https://www.canva.com/design/DAEDRNlAsoM/view?utm_content=DAEDRNlAsoM&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink`,
  },
  production: {
    MEDIA_BASE_URL: `https://www.theuy.nl/freeletics-media/`,
    VIDEOS_BASE_URL: `https://www.theuy.nl/freeletics-media/videos/`,
    THUMBS_BASE_URL: `https://www.theuy.nl/freeletics-media/thumbs/`,
    WORKOUT_COUNTDOWN_SECONDS: 5,
    GRAPHQL_ENDPOINT_URI: `https://www.theuy.nl/freeletics-api/v1/graphql`,
    DIGITAL_PORTFOLIO_LINK: `https://www.canva.com/design/DAEDRNlAsoM/view?utm_content=DAEDRNlAsoM&utm_campaign=designshare&utm_medium=link&utm_source=publishsharelink`,
  },
} as any;

export const MEDIA_BASE_URL = configs[process.env.NODE_ENV].MEDIA_BASE_URL;
export const VIDEOS_BASE_URL = configs[process.env.NODE_ENV].VIDEOS_BASE_URL;
export const THUMBS_BASE_URL = configs[process.env.NODE_ENV].THUMBS_BASE_URL;
export const WORKOUT_COUNTDOWN_SECONDS =
  configs[process.env.NODE_ENV].WORKOUT_COUNTDOWN_SECONDS;
export const GRAPHQL_ENDPOINT_URI =
  configs[process.env.NODE_ENV].GRAPHQL_ENDPOINT_URI;
export const DIGITAL_PORTFOLIO_LINK =
  configs[process.env.NODE_ENV].DIGITAL_PORTFOLIO_LINK;

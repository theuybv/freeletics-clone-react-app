import useWorkoutsByPk from "../../hooks/useWorkoutByPk";
import React from "react";
import WorkoutCountDown from "../../pages/WorkoutCountDown";
import WorkoutDetails from "../../pages/WorkoutDetails";
import { useRouteMatch } from "react-router";
import ActiveWorkoutExercise from "../../pages/ActiveWorkoutExercise";
import useQueryFetchingError from "../../hooks/useQueryFetchingError";
import { QUERY_WORKOUTS } from "../../gqls/queries";
import ErrorBoundary from "../ErrorBoundary";
import {
  IonPage,
  IonContent,
  IonText,
  IonCard,
  IonImg,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
} from "@ionic/react";
import WorkoutsSlides from "../WorkoutsSlides";

// /workouts/:workoutId/countdown
export const WorkoutCountDownFetchComp = () => {
  const result = useWorkoutsByPk();
  const { data, fetching, error } = result;

  if (fetching) return null;
  if (error) return null;

  return (
    <WorkoutCountDown
      startExerciseName={data.workouts_by_pk.rounds[0].exercise.name}
    />
  );
};

// /workouts/:workoutId
export const WorkoutDetailsFetchComp = () => {
  const result = useWorkoutsByPk();

  const { data, fetching, error } = result;

  if (fetching) return null;
  if (error) return null;

  return <WorkoutDetails workout={data.workouts_by_pk} />;
};

// /workouts/:workoutId/round/:roundNumber/:exerciseId
export const ActiveWorkoutExerciseFetchComp = () => {
  const result = useWorkoutsByPk();

  const {
    params: { exerciseId, roundNumber },
  } = useRouteMatch();

  const { data, fetching, error } = result;

  if (fetching) return null;
  if (error) return null;

  return (
    <ActiveWorkoutExercise
      workout={data.workouts_by_pk}
      roundNumber={roundNumber}
      exerciseId={exerciseId}
    />
  );
};

export const ExploreTabFetchComp = () => {
  const [result] = useQueryFetchingError({
    query: QUERY_WORKOUTS,
  });

  const { fetching, data } = result;

  if (fetching) return null;

  return (
    <ErrorBoundary>
      <IonPage>
        <IonContent class="ion-no-padding">
          <section className="abstract-header">
            <div style={{ marginLeft: 16 }}>
              <IonText>
                <h1 style={{ fontWeight: "bold" }}>Workouts</h1>
              </IonText>
              <IonText>My Favourite HIT Workouts</IonText>
            </div>
            <WorkoutsSlides
              workouts={data ? (data.workouts ? data.workouts : []) : []}
            />
          </section>
          <IonCard className="explore-card">
            <IonImg src="/assets/voorkant-profiel.png" />
            <IonCardHeader>
              <IonCardSubtitle>Need digital products creating?</IonCardSubtitle>
              <IonCardTitle>hello@theuy.nl</IonCardTitle>
            </IonCardHeader>
          </IonCard>
        </IonContent>
      </IonPage>
    </ErrorBoundary>
  );
};

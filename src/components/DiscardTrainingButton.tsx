import React from "react";
import { IonButtons, IonIcon, useIonRouter, IonButton } from "@ionic/react";
import { closeOutline } from "ionicons/icons";
import useDialog from "../hooks/useDialog";

const DiscardTrainingButton: React.FC = () => {
  const { dialog, openDialog } = useDialog();
  const router = useIonRouter();
  return (
    <React.Fragment>
      {dialog}
      <IonButtons slot="start">
        <IonButton
          onClick={() => {
            openDialog({
              header: `Discard training?`,
              message: `Your training has not been saved yet and will be lost!`,
              buttons: [
                {
                  text: "No",
                  handler: () => {},
                },
                {
                  text: "Yes",
                  handler: () => {
                    router.push(`/explore`);
                  },
                },
              ],
            });
          }}
        >
          <IonIcon size="large" icon={closeOutline}></IonIcon>
        </IonButton>
      </IonButtons>
    </React.Fragment>
  );
};

export default DiscardTrainingButton;

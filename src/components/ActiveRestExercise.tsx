import React, { useEffect } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { IonText } from "@ionic/react";
import { useDispatch } from "react-redux";
import { useTimer } from "react-timer-hook";
import moment, { duration } from "moment";

const ActiveRestExercise: React.FC<{
  exerciseSeconds: number;
  onCounterCompleted: () => void;
}> = ({ exerciseSeconds, onCounterCompleted }) => {
  const dispatch = useDispatch();
  const expiryTimestamp = Number(
    moment()
      .add(exerciseSeconds + 1, "seconds")
      .format("x")
  );

  const restTimer = useTimer({
    expiryTimestamp: expiryTimestamp,
    onExpire: () => {
      const timeout = setTimeout(() => {
        clearTimeout(timeout);
        onCounterCompleted();
      }, 1000);
    },
  });

  const { seconds, minutes, hours } = restTimer;

  const _duration: moment.Duration = duration({ seconds, minutes, hours });
  const progressSeconds = _duration.asMilliseconds() / 1000;

  useEffect(() => {
    dispatch({
      type: "UPDATE_REST_TIMER",
      payload: { restTimer },
    });
  }, [restTimer]);

  return (
    <React.Fragment>
      <section style={{ width: "80%", position: "absolute", top: 80 + 30 }}>
        <IonText>
          <h1>Rest</h1>
        </IonText>
        <IonText style={{ fontSize: 60, fontWeight: "bold" }}>
          {progressSeconds}
        </IonText>
        <IonText style={{ fontSize: 30 }}>s</IonText>
      </section>
      <section style={{ width: "80%", position: "absolute", top: 80 }}>
        <CircularProgressbar
          counterClockwise={true}
          circleRatio={0.5}
          value={(progressSeconds / exerciseSeconds) * 100}
          text={""}
          strokeWidth={4}
          styles={buildStyles({
            // Rotation of path and trail, in number of turns (0-1)
            rotation: 0.25,

            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
            strokeLinecap: "butt",

            // Text size
            textSize: "30px",

            // How long animation takes to go from one percentage to another, in seconds
            pathTransitionDuration: 0.5,

            // Can specify path transition in more detail, or remove it entirely
            // pathTransition: 'none',

            // Colors
            pathColor: "#F5FFFF",
            textColor: "white",
            trailColor: "#3880FF",
            backgroundColor: "white",
          })}
        />
      </section>
    </React.Fragment>
  );
};

export default ActiveRestExercise;

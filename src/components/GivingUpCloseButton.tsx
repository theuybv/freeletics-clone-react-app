import React from "react";
import { IonIcon, useIonRouter, IonButton } from "@ionic/react";
import { closeOutline } from "ionicons/icons";
import useDialog from "../hooks/useDialog";

const GivingUpCloseButton: React.FC = () => {
  const { dialog, openDialog } = useDialog();
  const router = useIonRouter();
  return (
    <React.Fragment>
      {dialog}
        <IonButton
          onClick={() => {
            openDialog({
              header: `Giving Up is not an option!`,
              message: `Do you really want to cancel your training?`,
              buttons: [
                {
                  text: "No",
                  handler: () => {},
                },
                {
                  text: "Yes",
                  handler: () => {
                    router.push("/explore");
                  },
                },
              ],
            });
          }}
        >
          <IonIcon size="large" icon={closeOutline}></IonIcon>
        </IonButton>
    </React.Fragment>
  );
};

export default GivingUpCloseButton;

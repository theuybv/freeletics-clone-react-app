import React, { useEffect } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { IonText } from "@ionic/react";
import { useDispatch } from "react-redux";
import { useTimer } from "react-timer-hook";
import { Exercise } from "../gqls/types";
import { formatters } from "../utils";
import moment, { duration } from "moment";

const ActiveSecondsExercise: React.FC<{
  exerciseSeconds: number;
  onCounterCompleted: () => void;
  exercise: Exercise;
}> = ({ exerciseSeconds, onCounterCompleted, exercise: { title, name } }) => {
  const dispatch = useDispatch();
  const expiryTimestamp = Number(
    moment()
      .add(exerciseSeconds + 1, "seconds")
      .format("x")
  );

  const exerciseTimer = useTimer({
    expiryTimestamp,
    onExpire: () => {
      const timeout = setTimeout(() => {
        clearTimeout(timeout);
        onCounterCompleted();
      }, 1000);
    },
  });

  const { seconds, minutes, hours } = exerciseTimer;

  const _duration: moment.Duration = duration({ seconds, minutes, hours });
  const progressSeconds = _duration.asMilliseconds() / 1000;

  useEffect(() => {
    dispatch({
      type: "UPDATE_EXERCISE_TIMER",
      payload: { exerciseTimer },
    });
  }, [exerciseTimer]);

  return (
    <React.Fragment>
      <div
        style={{
          position: "absolute",
          top: 0,
          width: "100%",
          height: 400,
          background: `linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1) 42%, rgba(0,0,0,0) 100%),
    url(${formatters.thumbFileUrl(name)}) no-repeat top left`,
        }}
      ></div>
      <section style={{ width: "80%", position: "absolute", bottom: 48 }}>
        <IonText style={{ fontSize: 60, fontWeight: "bold" }}>
          {progressSeconds}
        </IonText>
        <IonText style={{ fontSize: 30 }}>s</IonText>
        <IonText>
          <h1 style={{ marginTop: 0 }}>{title}</h1>
        </IonText>
      </section>
      <section
        style={{
          width: 200,
          position: "absolute",
          left: "calc(50% - 200px /2",
          bottom: 0,
        }}
      >
        <CircularProgressbar
          counterClockwise={true}
          circleRatio={0.5}
          value={(progressSeconds / exerciseSeconds) * 100}
          text={""}
          strokeWidth={4}
          styles={buildStyles({
            // Rotation of path and trail, in number of turns (0-1)
            rotation: 0.25,

            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
            strokeLinecap: "butt",

            // Text size
            textSize: "30px",

            // How long animation takes to go from one percentage to another, in seconds
            pathTransitionDuration: 0.5,

            // Can specify path transition in more detail, or remove it entirely
            // pathTransition: 'none',

            // Colors
            pathColor: "#F5FFFF",
            textColor: "white",
            trailColor: "#3880FF",
            backgroundColor: "white",
          })}
        />
      </section>
    </React.Fragment>
  );
};

export default React.memo(ActiveSecondsExercise);

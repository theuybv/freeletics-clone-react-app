import React from "react";
import {
  IonToolbar,
  IonButtons,
  IonIcon,
  IonButton,
  IonHeader,
} from "@ionic/react";
import { closeOutline } from "ionicons/icons";
import { Exercise } from "../gqls/types";
import { formatters, fallbackImage } from "../utils";

const ExerciseVideoModal: React.FC<{
  onClose: () => void;
  exercise: Exercise;
}> = ({ onClose, exercise: { name } }) => {
  return (
    <React.Fragment>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton onClick={onClose}>
              <IonIcon icon={closeOutline} size="large" />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <video
        width={"100%"}
        height={"100%"}
        autoPlay
        controls
        poster={`${formatters.thumbFileUrl(name)}`}
        onError={fallbackImage}
      >
        <source src={`${formatters.videoFileUrl(name)}`} type="video/mp4" />
        Your browser does not support the video tag.
      </video>
    </React.Fragment>
  );
};

export default ExerciseVideoModal;

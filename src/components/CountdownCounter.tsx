import React, { useEffect } from "react";
import { IonText } from "@ionic/react";
import { useTimer } from "react-timer-hook";

type CountdownCounterType = {
  onCounterCompleted: (callback?: any) => void;
  totalSeconds?: number;
};
const CountdownCounter: React.FC<CountdownCounterType> = ({
  onCounterCompleted,
  totalSeconds = 5,
}) => {
  const currentDate = new Date();
  currentDate.setSeconds(currentDate.getSeconds() + totalSeconds + 1);
  const { seconds, isRunning } = useTimer({
    expiryTimestamp: currentDate.getTime(),
  });

  useEffect(() => {
    if (!isRunning) {
      onCounterCompleted();
    }
    return () => {};
  }, [isRunning, onCounterCompleted]);

  return (
    <React.Fragment>
      <IonText className="counter-timer-text">{seconds}</IonText>
    </React.Fragment>
  );
};

export default CountdownCounter;

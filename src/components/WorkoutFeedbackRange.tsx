import React, { useState } from "react";
import { IonText, IonRange, IonLabel } from "@ionic/react";

const WorkoutFeedbackRange = () => {
  const feedbacks = [
    "Not great - I had tot replace at least one exercise",
    "Ok - I could perform all exercises, but didn't focus on technique",
    "Ok - I could perform all exercises, but didn't focus on technique",
    "Great - I could perform all repetitoins with correct technique",
    "Excellent - I could perform all repetitons, taking time to execute perfect technique",
  ];
  const defaultRangeValue = Math.round(feedbacks.length / 2 - 1);
  const [value, setvalue] = useState(defaultRangeValue);

  return (
    <div>
      <IonText>
        <p>{feedbacks[value]}</p>
      </IonText>
      <IonRange
        value={value}
        min={0}
        max={feedbacks.length - 1}
        color="secondary"
        step={1}
        onIonChange={(e: any) => {
          setvalue(e.target.value);
        }}
      >
        <IonLabel slot="start">Poor</IonLabel>
        <IonLabel slot="end">Excellent</IonLabel>
      </IonRange>
    </div>
  );
};

export default WorkoutFeedbackRange;

import React from "react";
import { IonLabel, IonItem, IonThumbnail, IonImg, IonIcon } from "@ionic/react";
import { Round } from "../gqls/types";
import { formatters, fallbackImage } from "../utils";
import { stopwatchOutline } from "ionicons/icons";

const ExercisesSummaryListItems: React.FC<{
  rounds: Round[];
  maxRounds: number;
}> = ({ rounds, maxRounds }) => {
  return (
    <React.Fragment>
      <div className="ion-padding-top"></div>
      {Array.from(Array(maxRounds)).map((item: any, index: number) => {
        const filteredRounds = rounds.filter(
          (round) => round.number === index + 1
        );
        const currentRound = filteredRounds[index]
          ? filteredRounds[index]
          : filteredRounds[0];
        return (
          <React.Fragment key={currentRound.number}>
            <IonItem lines="none">
              <IonLabel>
                <h2>
                  Round {currentRound.number}/ {maxRounds}
                </h2>
              </IonLabel>
            </IonItem>
            {filteredRounds.map((round) => {
              const {
                exercise: { name, title },
              } = round;
              return (
                <IonItem key={name}>
                  <IonThumbnail style={{ width: 60, height: 60 }} slot="start">
                    {name === "rest" ? (
                      <IonIcon
                        icon={stopwatchOutline}
                        style={{
                          background: "black",
                          width: "100%",
                          height: "100%",
                        }}
                      />
                    ) : (
                      <IonImg
                        onIonError={fallbackImage}
                        src={`${formatters.thumbFileUrl(name)}`}
                        alt={`${name}`}
                      />
                    )}
                  </IonThumbnail>
                  <IonLabel>
                    {formatters.repetitionsOrSeconds(round)} {title}
                  </IonLabel>
                </IonItem>
              );
            })}
          </React.Fragment>
        );
      })}
    </React.Fragment>
  );
};

export default ExercisesSummaryListItems;

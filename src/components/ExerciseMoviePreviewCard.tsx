import React from "react";
import { IonIcon } from "@ionic/react";
import { playCircle } from "ionicons/icons";
import { Exercise } from "../gqls/types";
import { formatters, fallbackImage } from "../utils";
const ExerciseMoviePreviewCard: React.FC<{
  onClick: () => void;
  exercise: Exercise;
  imageLoaded?: () => void;
}> = ({ onClick, exercise: { name }, imageLoaded }) => {
  return (
    <React.Fragment>
      <div onClick={onClick}>
        <img
          alt={name}
          onError={fallbackImage}
          style={{ borderRadius: 8 }}
          src={`${formatters.thumbFileUrl(name)}`}
          onLoad={imageLoaded}
        />
        <div
          style={{
            position: "absolute",
            top: "calc(50% - (45px/2))",
            left: "calc(50% - (45px/2))",
          }}
        >
          <IonIcon style={{ fontSize: 45 }} icon={playCircle} />
        </div>
      </div>
    </React.Fragment>
  );
};

export default ExerciseMoviePreviewCard;

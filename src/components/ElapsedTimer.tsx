import React from "react";
import { IonText } from "@ionic/react";
import { padStart } from "lodash";
import { useSelector, shallowEqual } from "react-redux";
import { AppState } from "../reducers/appReducer";
type Timer = {
  hours?: number;
  seconds?: number;
  minutes?: number;
};
const ElapsedTimer: React.FC = () => {
  const { workoutStopWatch } = useSelector((state: AppState) => {
    return {
      workoutStopWatch: state.workoutStopWatch,
    };
  }, shallowEqual);
  const { hours = 0, seconds = 0, minutes = 0 } = workoutStopWatch;

  return (
    <IonText className="counter-timer-text" style={{ fontSize: 80 }}>
      {hours >= 1 && <span>{padStart(hours.toString(), 1, "0")}:</span>}
      <span>{padStart(minutes.toString(), 2, "0")}:</span>
      <span>{padStart(seconds.toString(), 2, "0")}</span>
    </IonText>
  );
};

export default React.memo(ElapsedTimer);

import React from "react";
import { IonText } from "@ionic/react";
import { Exercise } from "../gqls/types";
import { formatters } from "../utils";

const ActiveRepetitionExercise: React.FC<{
  exercise: Exercise;
  workoutMode: string;
}> = ({ exercise: { name, title }, workoutMode }) => {
  return (
    <React.Fragment>
      <div
        style={{
          position: "absolute",
          top: 0,
          width: "100%",
          height: 400,
          background: `linear-gradient(0deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1) 42%, rgba(0,0,0,0) 100%),
    url(${formatters.thumbFileUrl(name)}) no-repeat top left`,
        }}
      >
      </div>
      <div
        style={{
          position: "absolute",
          top: "50%",
          paddingTop: 160,
        }}
      >
        <IonText style={{ fontSize: 30, fontWeight: "bold" }}>
          <div>{workoutMode}</div>
        </IonText>
        <IonText style={{ fontSize: 24 }}>
          <div>{title}</div>
        </IonText>
      </div>
    </React.Fragment>
  );
};

export default ActiveRepetitionExercise;

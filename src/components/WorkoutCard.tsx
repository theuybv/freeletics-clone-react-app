import React from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon,
} from "@ionic/react";
import { ellipse, ellipseOutline } from "ionicons/icons";
import { Workout } from "../gqls/types";

const WorkoutCard: React.FC<{ workout: Workout }> = ({ workout }) => {
  const { name, title, duration, targets, difficulty } = workout;
  return (
    <IonCard className={"ion-text-start"} href={`/workouts/${name}`}>
      <IonCardHeader>
        <IonCardTitle>{title}</IonCardTitle>
        <IonCardSubtitle>{targets}</IonCardSubtitle>
      </IonCardHeader>
      <IonCardContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              Duration{" "}
              {Array.from(Array(3)).map((item: any, index: number) => {
                const icon = duration > index ? ellipse : ellipseOutline;
                return <IonIcon key={index} icon={icon}></IonIcon>;
              })}
            </IonCol>
            <IonCol>
              Difficulty{" "}
              {Array.from(Array(3)).map((item: any, index: number) => {
                const icon = difficulty > index ? ellipse : ellipseOutline;
                return <IonIcon key={index} icon={icon}></IonIcon>;
              })}
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonCardContent>
    </IonCard>
  );
};

export default WorkoutCard;

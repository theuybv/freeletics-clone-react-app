import React from "react";
import { IonSlides, IonSlide, IonText } from "@ionic/react";
import { Workout } from "../gqls/types";
import WorkoutCard from "./WorkoutCard";

const WorkoutsSlides: React.FC<{ workouts: Workout[] }> = ({ workouts }) => {
  return (
    <IonSlides
      options={{
        slidesPerView: 1.5,
      }}
    >
      {workouts.length ? workouts.map((workout: Workout) => {
        return (
          <IonSlide key={workout.id}>
            <WorkoutCard workout={workout} />
          </IonSlide>
        );
      }) : <IonText className="ion-padding"><h4>There are no workouts</h4></IonText>}
    </IonSlides>
  );
};

export default WorkoutsSlides;

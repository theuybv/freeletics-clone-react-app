import React from "react";
import { IonProgressBar } from "@ionic/react";

const AppLoader = () => {
  return (
    <div
      className="ion-justify-content-center ion-align-items-center"
      style={{ height: "100vh", display: "flex", padding: "0 40px" }}
    >
      <IonProgressBar type="indeterminate" />
    </div>
  );
};

export default AppLoader;

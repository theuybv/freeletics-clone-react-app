import React, { useEffect } from "react";
import {
  IonList,
  IonListHeader,
  IonItem,
  IonThumbnail,
  IonLabel,
  IonProgressBar,
  IonText,
  IonIcon,
  IonImg,
} from "@ionic/react";
import { Exercise } from "../gqls/types";
import { motion, useAnimation } from "framer-motion";
import { stopwatchOutline } from "ionicons/icons";
import { formatters, fallbackImage } from "../utils";

type ExtendedExerise = Exercise & {
  workoutDetails: string;
};
const ActiveProgressNextExercise: React.FC<{
  nextExercise: ExtendedExerise;
  progress: number;
  isFinalExercise: boolean;
}> = ({ nextExercise, progress, isFinalExercise }) => {
  const controls = useAnimation();

  useEffect(() => {
    (async () => {
      await controls.start({
        y: 200,
        opacity: 0,
        transition: {
          duration: 0,
        },
      });
      await controls.start({
        y: 0,
        opacity: 1,
        transition: {
          duration: 0.4,
        },
      });
    })();
  }, [progress, controls]);

  return (
    <React.Fragment>
      <IonList lines="none">
        {!isFinalExercise ? (
          <section>
            <IonListHeader style={{ fontSize: 16, paddingBottom: 4 }}>
              Next
            </IonListHeader>
            <motion.div animate={controls}>
              <IonItem>
                <IonThumbnail style={{ width: 60, height: 60 }} slot="start">
                  {nextExercise.name !== "rest" ? (
                    <IonImg
                      onIonError={fallbackImage}
                      src={`${formatters.thumbFileUrl(nextExercise.name)}`}
                      alt={nextExercise.name}
                    />
                  ) : (
                    <IonIcon
                      icon={stopwatchOutline}
                      style={{
                        background: "black",
                        width: "100%",
                        height: "100%",
                      }}
                    />
                  )}
                </IonThumbnail>
                <IonLabel>{nextExercise.workoutDetails}</IonLabel>
              </IonItem>
            </motion.div>
          </section>
        ) : (
          <section>
            <IonListHeader style={{ fontSize: 16 }}>
              <IonText
                className="ion-text-uppercase ion-text-center"
                style={{ width: "100%" }}
              >
                Final Exercise
              </IonText>
            </IonListHeader>
          </section>
        )}
        <br />
        <IonProgressBar
          value={progress}
          style={{ height: 12 }}
        ></IonProgressBar>
      </IonList>
    </React.Fragment>
  );
};

export default ActiveProgressNextExercise;

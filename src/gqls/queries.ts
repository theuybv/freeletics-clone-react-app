export const QUERY_WORKOUTS = `
query {
  workouts(order_by:{name:asc}) {
    name
    id
    title
    duration
    difficulty
  }
}
`;

export const QUERY_WORKOUT_BY_PK = `
query ($name:workout_names_enum!) {
  workouts_by_pk(name: $name) {
    name
    id
    title
    duration
    difficulty
    targets
    rounds_aggregate {
      aggregate{
        max {
          number
        }
      }
    }
    rounds{
      id
      number
      exerciseSeconds
      repetitionsCount
      exercise {
        id
        name
        title
        id
      }
    }
  }
}
`;

export const QUERY_WORKOUT_BY_NAME_ROUND_NUMBER_EXERCISE_NAME = `
  query (
    $workoutName:workout_names_enum!, 
    $exerciseName: exercise_names_enum!, 
    $roundNumber: Int!) {
    workouts_by_pk(name: $workoutName) {
      name
      id
      title
      duration
      difficulty
      targets
      rounds_aggregate {
        aggregate{
          max {
            number
          }
        }
      }
      rounds(where:{number:{_eq:$roundNumber}, exercise:{name:{_eq: $exerciseName}}}) {
        number
        id
        exerciseSeconds
        repetitionsCount
        exercise {
          name
          title
          id
        }
      }
    }
  }
`;

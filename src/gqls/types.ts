export interface Workout {
  name: string;
  id: string;
  title: string;
  duration: number;
  difficulty: number;
  targets: string;
  rounds_aggregate: Roundsaggregate;
  rounds: Round[];
}

export interface Round {
  number: number;
  id: string;
  exerciseSeconds?: any;
  repetitionsCount: number;
  exercise: Exercise;
}

export interface Exercise {
  name: string;
  title: string;
  id: string;
}

export interface Roundsaggregate {
  aggregate: Aggregate;
}

export interface Aggregate {
  max: Max;
}

export interface Max {
  number: number;
}

import React from "react";
import {
  IonPage,
  IonContent,
  IonToolbar,
  IonSlides,
  IonSlide,
  IonButton,
  IonButtons,
  IonIcon,
  useIonRouter,
  IonHeader,
  IonFooter,
  IonText,
  IonRouterLink,
} from "@ionic/react";
import GivingUpCloseButton from "../components/GivingUpCloseButton";
import ElapsedTimer from "../components/ElapsedTimer";
import { videocamOutline } from "ionicons/icons";
import ActiveProgressNextExercise from "../components/ActiveProgressNextExercise";
import ActiveRepetitionExercise from "../components/ActiveRepetitionExercise";
import useModal from "../hooks/useModal";
import ExerciseVideoModal from "../components/ExerciseVideoModal";
import { Workout } from "../gqls/types";
import { formatters, getMouseDirection } from "../utils";
import ActiveRestExercise from "../components/ActiveRestExercise";
import { chain, round } from "lodash";
import ActiveSecondsExercise from "../components/ActiveSecondsExercise";

let slideRef: any;

const ActiveWorkoutExercise: React.FC<{
  workout: Workout;
  roundNumber: number;
  exerciseId: string;
}> = ({ workout, roundNumber, exerciseId }) => {
  const router = useIonRouter();
  const { modal: VideoMmodal, openModal, closeModal } = useModal();
  const { rounds } = workout;

  const roundsGroupByNumber = chain(rounds).groupBy("number").value();
  const activeRound = chain(roundsGroupByNumber[roundNumber])
    .find((round) => round.exercise.name.toString() === exerciseId.toString())
    .value();

  if (!activeRound)
    return (
      <IonContent className="ion-padding">
        <IonRouterLink href="/">
          <IonText style={{ color: "white" }}>
            No content found, go to home
          </IonText>
        </IonRouterLink>
      </IonContent>
    );

  const activeExercise = activeRound.exercise;
  const currentExerciseIndex = chain(rounds)
    .findIndex(
      (round) =>
        round.number.toString() === roundNumber.toString() &&
        round.exercise.name.toString() === exerciseId.toString()
    )
    .value();
  const isFinalExercise = currentExerciseIndex >= rounds.length - 1;
  const nextExerciseIndex = isFinalExercise
    ? round.length - 1
    : currentExerciseIndex + 1;
  const nextRound = rounds[nextExerciseIndex];

  const progress = currentExerciseIndex / (rounds.length - 1);

  return (
    <IonPage>
      {VideoMmodal}
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <GivingUpCloseButton />
          </IonButtons>
          <IonButtons slot="end">
            {activeExercise.name !== "rest" && (
              <IonButton
                onClick={() =>
                  openModal({
                    modalContent: (
                      <ExerciseVideoModal
                        exercise={activeExercise}
                        onClose={() => closeModal()}
                      />
                    ),
                  })
                }
              >
                <IonIcon icon={videocamOutline}></IonIcon>
              </IonButton>
            )}
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div
          style={{
            position: "fixed",
            top: "50%",
            zIndex: 3,
            display: "flex",
            width: "100%",
            justifyContent: "center",
            pointerEvents: "none",
          }}
        >
          {activeExercise.name !== "rest" && activeRound.repetitionsCount && (
            <ElapsedTimer />
          )}
        </div>

        <IonSlides
          ref={(ref: any) => (slideRef = ref)}
          className="active-exercise"
          onIonSlidesDidLoad={async (e: any) => {
            await e.target.slideTo(currentExerciseIndex);
            const excercise = rounds[currentExerciseIndex].exercise;
            const round = rounds[currentExerciseIndex];
            if (excercise.name === "rest" || round.exerciseSeconds) {
              await e.target.lockSwipeToNext(true);
            } else {
              await e.target.lockSwipeToNext(false);
            }
          }}
          onIonSlideTouchEnd={async (e: any) => {
            const isEnd = await e.target.isEnd();
            if (isEnd && getMouseDirection(e) === "LEFT") {
              router.push(`/workouts/${workout.name}/done`);
            }
          }}
          onIonSlideTransitionEnd={async (e: any) => {
            const index = await e.target.getActiveIndex();
            const excercise = rounds[index].exercise;
            const round = rounds[index];
            if (excercise.name === "rest" || round.exerciseSeconds) {
              await e.target.lockSwipeToNext(true);
            } else {
              await e.target.lockSwipeToNext(false);
            }
            router.push(
              `/workouts/${workout.name}/round/${rounds[index].number}/${rounds[index].exercise.name}`
            );
          }}
        >
          {rounds.map((round, index: number) => {
            return (
              <IonSlide key={round.exercise.name + index}>
                {activeRound.exercise.name === "rest" ||
                (activeRound.exerciseSeconds &&
                  activeRound.number === round.number &&
                  activeRound.exercise.name === round.exercise.name) ? (
                  round.exercise.name === "rest" ? (
                    <ActiveRestExercise
                      exerciseSeconds={activeRound.exerciseSeconds}
                      onCounterCompleted={async () => {
                        if (isFinalExercise) {
                          router.push(`/workouts/${workout.name}/done`);
                        } else {
                          await slideRef.lockSwipeToNext(false);
                          await slideRef.slideTo(currentExerciseIndex + 1);
                        }
                      }}
                    />
                  ) : (
                    <ActiveSecondsExercise
                      exerciseSeconds={activeRound.exerciseSeconds}
                      exercise={round.exercise}
                      onCounterCompleted={async () => {
                        if (isFinalExercise) {
                          router.push(`/workouts/${workout.name}/done`);
                        } else {
                          await slideRef.lockSwipeToNext(false);
                          await slideRef.slideTo(currentExerciseIndex + 1);
                        }
                      }}
                    />
                  )
                ) : (
                  <ActiveRepetitionExercise
                    exercise={round.exercise}
                    workoutMode={formatters.repetitionsOrSeconds(round)}
                  />
                )}
              </IonSlide>
            );
          })}
        </IonSlides>
      </IonContent>
      <IonFooter>
        <div className="ion-padding-bottom">
          <ActiveProgressNextExercise
            isFinalExercise={isFinalExercise}
            nextExercise={{
              ...nextRound.exercise,
              workoutDetails: `${formatters.repetitionsOrSeconds(nextRound)}  ${
                nextRound.exercise.title
              }`,
            }}
            progress={progress}
          />
        </div>
      </IonFooter>
    </IonPage>
  );
};

export default ActiveWorkoutExercise;

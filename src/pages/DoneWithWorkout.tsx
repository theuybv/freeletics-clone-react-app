import React from "react";
import {
  IonContent,
  IonPage,
  IonToolbar,
  IonText,
  IonButton,
  useIonRouter,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import DiscardTrainingButton from "../components/DiscardTrainingButton";

const DoneWithWorkout: React.FC = () => {
  const router = useIonRouter();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <DiscardTrainingButton />
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <section
          style={{
            display: "flex",
            flexDirection: "column",
            height: window.innerHeight - 80,
            textAlign: "center",
          }}
        >
          <div style={{ flexGrow: 3 }}>
            <IonText>
              <h3>
                <strong>
                  Nice job. You're done. Time to give your Coach feedback.
                </strong>
              </h3>
            </IonText>
            <IonText>
              <p>Finish accidentally?</p>
            </IonText>
            <div>
              <IonButton
                fill="outline"
                color="dark"
                onClick={() => router.back()}
              >
                Resume
              </IonButton>
            </div>
          </div>

          <div style={{ flexGrow: 1 }}>
            <IonText>
              <p>
                Your Coach customizes your training intensity based on the
                feedback you give
              </p>
            </IonText>
          </div>
        </section>
      </IonContent>
      <IonFooter>
        <div className="ion-padding">
          <IonButton expand="full" onClick={() => {
            router.push("feedback")
          }}>
            Give Coach feedback
          </IonButton>
        </div>
      </IonFooter>
    </IonPage>
  );
};

export default DoneWithWorkout;

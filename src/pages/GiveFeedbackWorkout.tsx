import React from "react";
import {
  IonPage,
  IonContent,
  IonToolbar,
  IonHeader,
  IonTitle,
  IonText,
  IonButton,
  IonFooter,
  useIonRouter,
} from "@ionic/react";
import DiscardTrainingButton from "../components/DiscardTrainingButton";
import WorkoutFeedbackRange from "../components/WorkoutFeedbackRange";

const GiveFeedbackWorkout: React.FC = () => {
  const router = useIonRouter();
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <DiscardTrainingButton />
          <IonTitle>Your technique</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent class="ion-padding ion-text-center">
        <section
          style={{
            display: "flex",
            flexDirection: "column",
            height: window.innerHeight - 80,
          }}
        >
          <div style={{ flexGrow: 4 }}>
            <IonText>
              <h5>How was your technique?</h5>
            </IonText>
          </div>
          <div style={{ flexGrow: 4 }}>
            <WorkoutFeedbackRange />
          </div>
        </section>
      </IonContent>
      <IonFooter>
        <div className="ion-padding">
          <IonButton
            expand="full"
            onClick={() => {
              router.push("/");
            }}
          >
            Continue
          </IonButton>
        </div>
      </IonFooter>
    </IonPage>
  );
};

export default GiveFeedbackWorkout;

import React, { createRef } from "react";
import {
  IonPage,
  IonContent,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonList,
  IonItem,
  IonListHeader,
  IonSlides,
  IonSlide,
  IonButton,
  useIonRouter,
  IonHeader,
  IonFooter,
} from "@ionic/react";
import ExerciseMoviePreviewCard from "../components/ExerciseMoviePreviewCard";
import ExercisesSummaryListItems from "../components/ExercisesSummaryListItems";
import ExerciseVideoModal from "../components/ExerciseVideoModal";
import useModal from "../hooks/useModal";
import { Workout, Round } from "../gqls/types";
import { useRouteMatch } from "react-router";
import { uniqBy } from "lodash";

const WorkoutDetails: React.FC<{ workout: Workout }> = ({ workout }) => {
  const {
    params: { workoutId },
  } = useRouteMatch();
  const router = useIonRouter();

  const { modal: VideoModal, openModal, closeModal } = useModal();

  const rounds: Round[] = workout.rounds;
  const maxRounds = workout.rounds_aggregate.aggregate.max.number;
  const ref = createRef();
  return (
    <IonPage>
      {VideoModal}
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" />
          </IonButtons>
          <IonTitle>{workout.title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonList>
          <IonListHeader>What to know</IonListHeader>
          <IonItem>
            <IonSlides
              ref={ref as any}
              options={{
                spaceBetween: 20,
                slidesPerView: 1.5,
                preloadImages: true,
                lazy: true,
              }}
            >
              {uniqBy(
                rounds.filter((round) => round.exercise.name !== 'rest'),
                "exercise.name"
              ).map((round, index: number) => {
                const currentExercise = round.exercise;
                return (
                  <IonSlide key={index} className="video-preview">
                    <ExerciseMoviePreviewCard
                      exercise={currentExercise}
                      onClick={() => {
                        openModal({
                          modalContent: (
                            <ExerciseVideoModal
                              exercise={currentExercise}
                              onClose={() => closeModal()}
                            />
                          ),
                        });
                      }}
                    />
                  </IonSlide>
                );
              })}
            </IonSlides>
          </IonItem>
          <IonListHeader>Summary</IonListHeader>
          <ExercisesSummaryListItems rounds={rounds} maxRounds={maxRounds} />
        </IonList>
      </IonContent>
      <IonFooter>
        <div className="ion-padding">
          <IonButton
            expand="full"
            onClick={() => {
              router.push(`${workoutId}/countdown`);
            }}
          >
            Start
          </IonButton>
        </div>
      </IonFooter>
    </IonPage>
  );
};

export default WorkoutDetails;

import React from "react";
import {
  IonPage,
  IonContent,
  IonToolbar,
  useIonRouter,
  IonHeader,
  IonButtons,
} from "@ionic/react";
import CountdownCounter from "../components/CountdownCounter";
import GivingUpCloseButton from "../components/GivingUpCloseButton";
import { useSelector, shallowEqual } from "react-redux";
import { AppState } from "../reducers/appReducer";
import { WORKOUT_COUNTDOWN_SECONDS } from "../configs";

const WorkoutCountDown: React.FC<{ startExerciseName: string }> = ({
  startExerciseName,
}) => {
  const router = useIonRouter();

  const { workoutStopWatch } = useSelector((state: AppState) => {
    return {
      workoutStopWatch: state.workoutStopWatch,
    };
  }, shallowEqual);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <GivingUpCloseButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className="center-center">
          <CountdownCounter
            totalSeconds={WORKOUT_COUNTDOWN_SECONDS}
            onCounterCompleted={() => {
              workoutStopWatch.start();
              router.push(`round/1/${startExerciseName}`);
            }}
          />
        </div>
      </IonContent>
    </IonPage>
  );
};

export default WorkoutCountDown;

import { Round } from "./gqls/types";
import { THUMBS_BASE_URL, VIDEOS_BASE_URL } from "./configs";
import { kebabCase } from "lodash";
import { ReactEventHandler } from "react";
const statman = require("statman");

export const formatters = {
  repetitionsOrSeconds: (round: Round): string => {
    const { exerciseSeconds, repetitionsCount } = round;
    return exerciseSeconds ? exerciseSeconds + "s" : repetitionsCount + "x";
  },
  thumbFileUrl(name: string) {
    return `${THUMBS_BASE_URL}${kebabCase(name)}.jpg`;
  },
  videoFileUrl(name: string) {
    return `${VIDEOS_BASE_URL}${kebabCase(name)}.mp4`;
  },
};

type MouseDirection = "LEFT" | "RIGHT";
let lastX = 0;
export const getMouseDirection = (e: any): MouseDirection => {
  let direction: MouseDirection = "LEFT";
  let currentX = e.detail.changedTouches[0].clientX;

  if (currentX > lastX) {
    direction = "RIGHT";
  } else if (currentX < lastX) {
    direction = "LEFT";
  }
  lastX = currentX;
  return direction;
};

export const appStopWatch = new statman.Stopwatch();

export const fallbackImage = (e: CustomEvent | ReactEventHandler | any) => {
  const target = e.target as HTMLElement;
  target.setAttribute("src", "/assets/no-image.png");
};

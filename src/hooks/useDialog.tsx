import React from "react";
import { IonAlert, AlertButton } from "@ionic/react";
import { useStateIfMounted } from "use-state-if-mounted";

type DialogOptionsType = {
  header?: string;
  subHeader?: string;
  message?: string;
  buttons?: string[] | AlertButton[];
};
const defaultDialogOptions: DialogOptionsType = {
  header: "<header>",
  subHeader: "",
  message: "<message title>",
  buttons: [
    {
      text: "Cancel",
      role: "cancel",
      cssClass: "secondary",
      handler: () => {
        console.log("Confirm Cancel: blah");
      },
    },
    {
      text: "Okay",
      handler: () => {
        console.log("Confirm Okay");
      },
    },
  ],
};
const useDialog = () => {
  const [open, setOpen] = useStateIfMounted(false);
  const [dialogOptions, setDialogOptions] = useStateIfMounted(
    defaultDialogOptions
  );
  return {
    dialog: (
      <IonAlert
        isOpen={open}
        onDidDismiss={() => setOpen(false)}
        cssClass="dialog-alert"
        header={dialogOptions.header}
        subHeader={dialogOptions.subHeader}
        message={dialogOptions.message}
        buttons={dialogOptions.buttons}
      />
    ),
    openDialog: (dialogOptions: DialogOptionsType) => {
      setOpen(true);
      setDialogOptions({
        ...defaultDialogOptions,
        ...dialogOptions,
      });
    },
    closeDialog: () => setOpen(false),
  };
};

export default useDialog;

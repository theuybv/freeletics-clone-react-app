import React from "react";
import { IonModal } from "@ionic/react";
import { useStateIfMounted } from "use-state-if-mounted";

type ModaloptionsType = {
  modalContent?: JSX.Element;
};
const defaultModalOptions: ModaloptionsType = {
  modalContent: <div>This is the default modal content</div>,
};
const useModal = () => {
  const [open, setOpen] = useStateIfMounted(false);
  const [modalOptions, setModalOptions] = useStateIfMounted(
    defaultModalOptions
  );
  return {
    modal: (
      <IonModal
        isOpen={open}
        swipeToClose={true}
        onDidDismiss={() => setOpen(false)}
      >
        {modalOptions.modalContent}
      </IonModal>
    ),
    openModal: (modalOptions?: ModaloptionsType) => {
      setOpen(true);
      setModalOptions({
        ...defaultModalOptions,
        ...modalOptions,
      });
    },
    closeModal: () => setOpen(false),
  };
};

export default useModal;

import { useRouteMatch } from "react-router";
import { useQuery } from "urql";
import { QUERY_WORKOUT_BY_PK } from "../gqls/queries";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import useQueryFetchingError from "./useQueryFetchingError";

const useWorkoutsByPk = (workoutName?: string): any => {
  const {
    params: { workoutId },
  } = useRouteMatch();

  const [result] = useQueryFetchingError({
    query: QUERY_WORKOUT_BY_PK,
    variables: {
      name: workoutName ? workoutName : workoutId,
    },
  });

  return result;
};
export default useWorkoutsByPk;

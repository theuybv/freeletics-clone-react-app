import React from "react";
import { IonToast, ToastOptions } from "@ionic/react";
import { useStateIfMounted } from "use-state-if-mounted";

const defaultToastOptions: ToastOptions = {
  message: "Click to Close",
  position: "top",
  buttons: [
    {
      side: "start",
      icon: "star",
      text: "Favorite",
      handler: () => {
        console.log("Favorite clicked");
      },
    },
    {
      text: "Done",
      role: "cancel",
      handler: () => {
        console.log("Cancel clicked");
      },
    },
  ],
};
const useToast = () => {
  const [open, setOpen] = useStateIfMounted(false);
  const [toastOptions, setToastOptions] = useStateIfMounted(
    defaultToastOptions
  );
  return {
    toast: (
      <IonToast
        isOpen={open}
        onDidDismiss={() => {
          setOpen(false);
        }}
        message={toastOptions.message}
        position={toastOptions.position}
        buttons={toastOptions.buttons}
      />
    ),
    openToast: (toastOptions?: ToastOptions) => {
      setOpen(true);
      setToastOptions({
        ...defaultToastOptions,
        ...toastOptions,
      });
    },
    cloasToast: () => setOpen(false),
  };
};

export default useToast;

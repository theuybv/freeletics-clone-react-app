import { useRouteMatch } from "react-router";
import { useQuery } from "urql";
import {
  QUERY_WORKOUT_BY_PK,
  QUERY_WORKOUT_BY_NAME_ROUND_NUMBER_EXERCISE_NAME,
} from "../gqls/queries";
import useQueryFetchingError from "./useQueryFetchingError";

const useWorkoutByPkRoundNumberExerciseName = (): any => {
  const {
    params: { workoutId, roundNumber, exerciseId },
  } = useRouteMatch();

  const [result] = useQueryFetchingError({
    query: QUERY_WORKOUT_BY_NAME_ROUND_NUMBER_EXERCISE_NAME,
    variables: {
      workoutName: workoutId,
      roundNumber,
      exerciseName: exerciseId,
    },
  });

  return result;
};
export default useWorkoutByPkRoundNumberExerciseName;

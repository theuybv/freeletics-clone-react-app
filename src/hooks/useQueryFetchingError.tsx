import { useQuery, UseQueryArgs, UseQueryResponse } from "urql";
import { useDispatch } from "react-redux";
import { useEffect } from "react";

const useQueryFetchingError = (
  args: UseQueryArgs<object>
): UseQueryResponse<any> => {
  const useQueryResponse = useQuery(args);
  const { error, fetching, data } = useQueryResponse[0];
  const dispatch = useDispatch();

  useEffect(() => {
    if (fetching) {
      dispatch({ type: "SET_APP_IS_LOADING", payload: { appIsLoading: true } });
    } else {
      dispatch({
        type: "SET_APP_IS_LOADING",
        payload: { appIsLoading: false },
      });
    }
    return () => {};
  }, [fetching]);

  useEffect(() => {
    if (error) {
      dispatch({ type: "SET_APP_ERROR", payload: { appError: error } });
    } else {
      dispatch({
        type: "SET_APP_ERROR",
        payload: { appError: false },
      });
    }
    return () => {};
  }, [error]);
  return useQueryResponse;
};

export default useQueryFetchingError;

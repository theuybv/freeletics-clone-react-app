import { StopwatchResult, TimerResult } from "react-timer-hook";

export type AppState = {
  workoutStopWatch: StopwatchResult;
  restTimer: TimerResult;
  exerciseTimer: TimerResult;
  appIsLoading: boolean;
  appError: boolean | any;
};
const stopWatchDefault = {
  seconds: 0,
  minutes: 0,
  hours: 0,
  days: 0,
  isRunning: false,
  start: () => {},
  pause: () => {},
  reset: () => {},
};
const timerDefault = {
  seconds: 0,
  minutes: 0,
  hours: 0,
  days: 0,
  isRunning: false,
  start: () => {},
  pause: () => {},
  resume: () => {},
  restart: (newExpiryTimestamp: number) => {},
};
const defaultState: AppState = {
  workoutStopWatch: stopWatchDefault,
  restTimer: timerDefault,
  exerciseTimer: timerDefault,
  appIsLoading: false,
  appError: false,
};

const appReducer = (
  state: AppState = defaultState,
  action: { type: string; payload: AppState }
) => {
  switch (action.type) {
    case "UPDATE_WORKOUT_STOPWATCH":
      return {
        ...state,
        workoutStopWatch: {
          ...action.payload.workoutStopWatch,
        } as StopwatchResult,
      };
    case "UPDATE_REST_TIMER":
      return {
        ...state,
        restTimer: {
          ...action.payload.restTimer,
        } as TimerResult,
      };
    case "UPDATE_EXERCISE_TIMER":
      return {
        ...state,
        exerciseTimer: {
          ...action.payload.exerciseTimer,
        } as TimerResult,
      };
    case "SET_APP_IS_LOADING":
      return {
        ...state,
        appIsLoading: action.payload.appIsLoading,
      };
    case "SET_APP_ERROR":
      return {
        ...state,
        appError: action.payload.appError,
      };
    default:
      return state;
  }
};

export default appReducer;

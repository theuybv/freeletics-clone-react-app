# Docker Image which is used as foundation to create
# a custom Docker Image with this Dockerfile
FROM node:10

# A directory within the virtualized Docker environment
# Becomes more relevant when using Docker Compose later
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# Copies package.json and package-lock.json to Docker environment
COPY package*.json ./

# Installs all node packages
RUN yarn

# Copies everything over to Docker environment
COPY . .

# Uses port which is used by the actual application
# EXPOSE 8080

# RUN npm run build

# # Finally runs the application
# CMD [ "npx", "http-server", "/usr/src/app/build" ]

CMD ["npm", "start"]
